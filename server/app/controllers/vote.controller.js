const db = require("../models");
const Vote = db.votes;
const Op = db.Sequelize.Op;

// create and save a new user
exports.create = (req, res) => {
    // validate request
    if (!req.body.idVoter) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }

    // create a vote
    const vote = {
        idVoter: req.body.idVoter,
        publicVote: req.body.publicVote,
        idCandidate: req.body.idCandidate,
        idEvent: req.body.idEvent,
        dateCreation: req.body.dateCreation
    };

    // save vote in the database
    Vote.create(vote)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the vote."
        });
      });
  };

// retrieve all votes from the database.
exports.findAll = (req, res) => {
    const vote = req.query.vote;
    var condition = vote ? { vote: { [Op.like]: `%${vote}%` } } : null;

    Vote.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving votes."
        });
      });
  };

// find a single vote with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

    Vote.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving vote with id=" + id
      });
    });
};

// update a vote by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

    Vote.update(req.body, {
    where: { idVote: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: "vote was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update vote with id=${id}. Maybe vote was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating vote with id=" + id
      });
    });
};

// delete a vote with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

    Vote.destroy({
    where: { idVote: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: "vote was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete vote with id=${id}. Maybe vote was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete vote with id=" + id
      });
    });
};

// delete all users from the database.
exports.deleteAll = (req, res) => {
    Vote.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} votes were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all votes."
      });
    });
};

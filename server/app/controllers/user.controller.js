const db = require("../models");
const User = db.users;
const Op = db.Sequelize.Op;

// create and save a new user
exports.create = (req, res) => {
    // validate request
    if (!req.body.nameUser) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }

    // create a user
    const user = {
        nameUser: req.body.nameUser,
        emailUser: req.body.emailUser,
        passwordUser: req.body.passwordUser,
        identificationNumberUser: req.body.identificationNumberUser,
        typeOfDocumentUser: req.body.typeOfDocumentUser,
        accessCode: req.body.accessCode
    };

    // save user in the database
    User.create(user)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the user."
        });
      });
  };

// retrieve all users from the database.
exports.findAll = (req, res) => {
    const nameUser = req.query.nameUser;
    var condition = nameUser ? { nameUser: { [Op.like]: `%${nameUser}%` } } : null;
  
    User.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving users."
        });
      });
  };

// find a single user with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving user with id=" + id
      });
    });
};

// update a user by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  User.update(req.body, {
    where: { idUser: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update user with id=${id}. Maybe user was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating user with id=" + id
      });
    });
};

// delete a user with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { idUser: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete user with id=${id}. Maybe user was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete user with id=" + id
      });
    });
};

// delete all users from the database.
exports.deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Users were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all users."
      });
    });
};

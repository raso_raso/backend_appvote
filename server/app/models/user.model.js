module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
        idUser: {
            type: Sequelize.INTEGER,
            allowNull:false,
            autoIncrement: true,
            primaryKey: true,
            // will result in an attribute that is idUser when user facing but id_user in the database
            field: 'id_user'
        },
        nameUser: {
            type: Sequelize.STRING,
            field: 'name_user'
        },
        emailUser: {
            type: Sequelize.STRING,
            field: 'email_user'
        },
        passwordUser: {
            type: Sequelize.STRING,
            field: 'password'
        },
        identificationNumberUser: {
            type: Sequelize.STRING,
            field: 'nidentificacao_user'
        },
        typeOfDocumentUser: {
            type: Sequelize.STRING,
            field: 'type_document'
        },
        accessCode: {
            type: Sequelize.STRING,
            field: 'access_code'
        },
        createdUser: {
            type: Sequelize.DATE,
            field: 'created'
        }

    }, {
        // model tableName will be the same as the model name
        freezeTableName: true
      });

    return User;
  };

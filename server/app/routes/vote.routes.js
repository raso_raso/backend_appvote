module.exports = app => {
    const votes = require("../controllers/vote.controller.js");
  
    var router = require("express").Router();
  
    // create a new user
    router.post("/", votes.create);
  
    // retrieve all users
    router.get("/", votes.findAll);
    
    // retrieve a single user with id
    router.get("/:id", votes.findOne);
  
    // update a user with id
    router.put("/:id", votes.update);
  
    // delete a user with id
    router.delete("/:id", votes.delete);
  
    // delete all users
    router.delete("/", votes.deleteAll);
  
    app.use('/api/votes', router);
  };

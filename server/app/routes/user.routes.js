module.exports = app => {
    const users = require("../controllers/user.controller.js");
  
    var router = require("express").Router();
  
    // create a new user
    router.post("/", users.create);
  
    // retrieve all users
    router.get("/", users.findAll);
    
    // retrieve a single user with id
    router.get("/:id", users.findOne);
  
    // update a user with id
    router.put("/:id", users.update);
  
    // delete a user with id
    router.delete("/:id", users.delete);
  
    // delete all users
    router.delete("/", users.deleteAll);
  
    app.use('/api/users', router);
  };
